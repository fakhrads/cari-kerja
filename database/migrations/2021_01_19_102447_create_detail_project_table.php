<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_project', function (Blueprint $table) {
            $table->primary('id');
            $table->unsignedBigInteger('id_user');
            $table->unsignedBigInteger('id_project');
            $table->bigInteger('price');
            $table->longText('description');
            $table->boolean('status');
            $table->timestamps();

            $table->foreign('id_project')->references('id')->on('project');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_project');
    }
}
