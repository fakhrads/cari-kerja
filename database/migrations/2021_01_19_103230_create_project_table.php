<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project', function (Blueprint $table) {
            $table->primary('id_project');
            $table->unsignedBigInteger('id_user');
            $table->string('project_title', 50);
            $table->string('description', 100);
            $table->string('category', 100);
            $table->string('budget', 100);
            $table->boolean('project_status');
            $table->date('finish_time');
            $table->int('bid');
            $table->string('progress',25);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project');
    }
}
