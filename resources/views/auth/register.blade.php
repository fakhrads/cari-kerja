@extends('layouts.app')

@section('content')
<div class="padding-top-90 padding-bottom-90 access-page-bg">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <div class="access-form">
            <div class="form-header">
              <h5><i data-feather="user-plus"></i>Register</h5>
            </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('register') }}">
            @csrf
            
            <div class="form-group">
                <input id="name" placeholder="Nama Lengkap" class="form-control" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="form-group">
                <input id="email" placeholder="Email anda" class="form-control" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="form-group">
                <input id="password" placeholder="Password" class="form-control" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="form-group">
                <input id="password_confirmation" placeholder="Konfirmasi Password" class="form-control" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="more-option terms">
                <div class="mt-0 terms">
                  <input class="custom-radio" type="checkbox" id="radio-4" name="termsandcondition" checked>
                  <label for="radio-4">
                    <span class="dot"></span> I accept the <a href="#">terms & conditions</a>
                  </label>
                </div>
              </div>

            <div class="flex items-center justify-end mt-4">
                <button class="button primary-bg btn-block">
                    {{ __('Register') }}
                </button>
            </div>
        </form>

        <div class="shortcut-login">
            <span>Or connect with</span>
            <div class="buttons">
              <a href="#" class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
              <a href="#" class="google"><i class="fab fa-google"></i>Google</a>
            </div>
            <p>Don't have an account? <a href="register.html">Register</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
