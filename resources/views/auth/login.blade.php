@extends('layouts.app')

@section('content')
  <div class="padding-top-90 padding-bottom-90 access-page-bg">
    <div class="container">
      <div class="row">
        <div class="col-xl-4 col-md-6">
          <div class="access-form">
            <div class="form-header">
              <h5><i data-feather="user"></i>Login</h5>
            </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <input type="email" placeholder="Email Address" class="form-control" type="email" name="email" :value="old('email')" required autofocus >
            </div>

            <div class="form-group">
                <input type="password" placeholder="Password" class="form-control" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="more-option">
                <label for="remember_me" class="flex items-center">
                    <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>

                @if (Route::has('password.request'))
                <a class="" href="{{ route('password.request') }}">
                    {{ __('Forgot your password?') }}
                </a>
                @endif
            </div>

            <button type="submit" class="button primary-bg btn-block">
                {{ __('Login') }}
            </button>
        </form>
        <div class="shortcut-login">
            <span>Or connect with</span>
            <div class="buttons">
              <a href="#" class="facebook"><i class="fab fa-facebook-f"></i>Facebook</a>
              <a href="#" class="google"><i class="fab fa-google"></i>Google</a>
            </div>
            <p>Don't have an account? <a href="register.html">Register</a></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection