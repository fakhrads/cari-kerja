<header class="header-2 access-page-nav">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="header-top">
            <div class="logo-area">
              <a href="index.html"><img src="images/logo-2.png" alt=""></a>
            </div>
            <div class="top-nav">
              <a href="register.html" class="account-page-link">Register</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>