 <nav class="navbar navbar-expand-lg cp-nav-2">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="menu-item active"><a title="Home" href="{{ url('/') }}">Home</a></li>
        <li class="menu-item dropdown">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Dashboard</a>
          <ul  class="dropdown-menu">
            <li class="menu-item dropdown">
              <a href="#" data-toggle="dropdown"  class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Candidate Dashboard</a>
              <ul class="dropdown-menu">
                <li class="menu-item"><a  href="dashboard.html">Dashboard</a></li>
              </ul>
            </li>
            <li class="menu-item dropdown">
              <a href="#" data-toggle="dropdown"  class="dropdown-toggle" aria-haspopup="true" aria-expanded="false">Employer Dashboard</a>
              <ul class="dropdown-menu">
                <li class="menu-item"><a href="employer-dashboard.html">Employer Dashboard</a></li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="menu-item"><a href="{{ url('contact') }}">Contact Us</a></li>
        @if(Auth::check())
          <li class="menu-item post-job"><a href="my"><i class="fas fa-home"></i>Dashboard</a></li>
        @else
          <li class="menu-item post-job"><a href="post-job.html"><i class="fas fa-plus"></i>Register</a></li>
        @endif
      </ul>
    </div>
  </nav>