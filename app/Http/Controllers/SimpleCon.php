<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SimpleCon extends Controller
{
    //
    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect('/');
    }

    public function contact()
    {
        return view('contact',[
            'title' => 'Contact Us'
            ]);
    }
}
