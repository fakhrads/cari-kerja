<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\SimpleCon;
use App\Http\Controllers\DashboardController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/contact', [SimpleCon::class, 'contact']);

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('my', [DashboardController::class, 'index']);

    Route::get('user/profile', function () {
        // Uses first & second middleware...
    });
});

Route::get('/logout', [SimpleCon::class, 'logout']);
